## Standarts ##
#### PSR ####
* [PSR-1 [Basic Coding Standart]](http://www.php-fig.org/psr/psr-1/)
* [PSR-2 [Coding Style Guide]](http://www.php-fig.org/psr/psr-2/)
* [PSR-3 [Logger Interface]](http://www.php-fig.org/psr/psr-3/)
* [PSR-4 [Autoloading Standart]](http://www.php-fig.org/psr/psr-4/)
* [PSR-6 [Caching Interface]](http://www.php-fig.org/psr/psr-6/)
* [PSR-7 [HTTP Message Interface]](http://www.php-fig.org/psr/psr-7/)
* [PSR-11 [Container Interface]](http://www.php-fig.org/psr/psr-11/)
* [PSR-13 [Hypermedia Interface]](http://www.php-fig.org/psr/psr-13/)
* [PSR-16 [Simple Cache]](http://www.php-fig.org/psr/psr-16/)

#### Versioning ####
* [SEMVER](http://semver.org/lang/ru/)

#### Code documentation ####
##### PHPDoc #####
* [Getting started](https://docs.phpdoc.org/getting-started/index.html)
* [Documentation](https://docs.phpdoc.org/)

## Package management ##
### Composer ###
* [Install](https://getcomposer.org/download/)
* [Documentation](https://getcomposer.org/doc/)
* [Packagist](https://packagist.org/)

## Testing ##
#### Codeception ####
* [Quickstart](http://codeception.com/quickstart)
* [Documentation](http://codeception.com/docs/)

#### PHPUnit ####
* [Install](https://phpunit.de/manual/current/en/installation.html)
* [Documentation](https://phpunit.de/manual/current/en/index.html)

## Debug ##
#### Xdebug ####
* [Install](https://xdebug.org/download.php)
* [Documentation](https://xdebug.org/docs/)

## VCS ##
#### Git ####
* [Install](https://git-scm.com/downloads)
* [Documentation]()

## Best practice ##
* [PHP The Right Way](http://www.phptherightway.com/)
* [Git Flow](https://habrahabr.ru/post/106912/)